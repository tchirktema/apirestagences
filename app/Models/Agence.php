<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agence extends Model
{
    protected $fillable = [
        'nom',
        'situation_geographique',
        'ville',
        'longitude',
        'latitude',
        'category_id',
        'telephone',
        'image'
    ];
    public $timestamps = true;

    public function category(){
        return $this->belongsTo('App\Models\Category');
    }

    public static function validationRules(): array
    {
        $rules = [
            'nom' => 'required',
            'situation_geographique'=>'required',
            'ville'=>'required',
            'longitude'=>'required',
            'latitude'=>'required',
            'category_id'=> 'required',
            'telephone' => 'required',
            'image'=> 'required'
        ];

        return $rules;
    }
}
