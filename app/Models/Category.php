<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name','marker'];
    public $timestamps = true;

    public static function validationRules(): array
    {
        $rules = [
            'name'=>'required',
            'marker'=> 'required'
        ];

        return $rules;
    }

    public function agences(){
        return $this->hasMany('App\Models\Agence');
    }
}
