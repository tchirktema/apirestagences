<?php

namespace App\Http\Controllers\API\v1\Agence;

use Illuminate\Http\Request;
use App\Http\Controllers\API\v1\BaseController as BaseController;
use App\Models\Agence;
use Validator;

class AgenceController extends BaseController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $agences = Agence::all();
        return $this->sendResponse($agences->load('category')->toArray(),'Agences retrieved successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Agence $agence)
    {
        $input = $request->all();

        // get a validation rules
        $rules = Agence::validationRules();
        //make a validation
        $validator = Validator::make($input,$rules);

        if($validator->fails())
        {
            return $this->sendError('Validation Error.',$validator->errors());
        }

        $agence->nom = $input['nom'];
        $agence->situation_geographique = $input['situation_geographique'];
        $agence->ville = $input['ville'];
        $agence->telephone = $input['telephone'];
        $agence->longitude = $input['longitude'];
        $agence->latitude  = $input['latitude'];
        $agence->category_id = $input['category_id'];
        $agence->image = $input['image'];

        $agence->save();

        return $this->sendResponse($agence->toArray(),'Agence created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $agence = Agence::find($id);

        if(is_null($agence))
        {
            return $this->sendError('Agence not found');
        }

        return $this->sendResponse($agence->toArray(),'Agence retrieved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $agence = Agence::find($id);

        if(is_null($agence))
        {
            return $this->sendError('Agence not found');
        }

        $input = $request->all();

        $rules = Agence::validationRules();
        $validator = Validator::make($input,$rules);

        if($validator->fails())
        {
            return $this->sendError('Validation Error',$validator->errors());
        }

        $agence->nom = $input['nom'];
        $agence->situation_geographique = $input['situation_geographique'];
        $agence->ville = $input['ville'];
        $agence->telephone = $input['telephone'];
        $agence->longitude = $input['longitude'];
        $agence->latitude  = $input['latitude'];
        $agence->category_id = $input['category_id'];

        $agence->save();
        return $this->sendResponse($agence->toArray(),'Agence updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // check if Agence exist
        $agence = Agence::find($id);

        if(is_null($agence))
        {
            return $this->sendError('Agence not found');
        }
        $agence->delete();
        return $this->sendResponse($agence->toArray(), 'Agence deleted successfully.');
    }
}
