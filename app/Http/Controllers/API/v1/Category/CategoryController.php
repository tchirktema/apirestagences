<?php

namespace App\Http\Controllers\API\v1\Category;

use Illuminate\Http\Request;
use App\Http\Controllers\API\v1\BaseController as BaseController;
use App\Models\Category;
use Validator;

class CategoryController extends BaseController
{
    // public function __construct()
    // {
    //     $this->middleware('auth:api');
    // }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();

        return $this->sendResponse($categories->toArray(),'Categories retrieved successfully.');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Category $category)
    {
       
        $input = $request->all();

        // get a validation rules
        $rules = Category::validationRules();
        //make a validation
        $validator = Validator::make($input,$rules);

        if($validator->fails())
        {
            return $this->sendError('Validation Error.',$validator->errors());
        }

        $category->name = $input['name'];
        $category->marker = $input['marker'];
        $category->save();

        return $this->sendResponse($category->toArray(),'Category created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);

        if(is_null($category))
        {
            return $this->sendError('Category not found');
        }

        return $this->sendResponse($category->toArray(),'Category retrieved successfully.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // check if category exist
        $category = Category::find($id);

        if(is_null($category))
        {
            return $this->sendError('Category not found');
        }

        $input = $request->all();

        $rules = Category::validationRules();
        $validator = Validator::make($input,$rules);

        if($validator->fails())
        {
            return $this->sendError('Validation Error',$validator->errors());
        }

        $category->name = $input['name'];
        $category->marker = $input['marker'];
        $category->save();

        return $this->sendResponse($category->toArray(),'Category updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // check if category exist
        $category = Category::find($id);

        if(is_null($category))
        {
            return $this->sendError('Category not found');
        }
        $category->delete();
        return $this->sendResponse($category->toArray(), 'Category deleted successfully.');
    }
}
