<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



// Route::middleware('auth:api')->group( function () {
// 	Route::resource('v1/categories','API\v1\Category\CategoryController');
//     Route::resource('v1/agences','API\v1\Agence\AgenceController');
//     Route::post('logout', 'API\v1\Auth\AuthController@logout');
// });

Route::group(['middleware' => ['api']], function () {
    Route::post('v1/user-login', 'API\v1\User\LoginController@login');
    Route::post('v1/supplier-login', 'API\v1\Supplier\LoginController@login');
}); 

Route::resource('v1/agences','API\v1\Agence\AgenceController');

Route::apiResource('v1/categories','API\v1\Category\CategoryController');
Route::group(['middleware' => ['api', 'manage_token:api_user,ROLE_USER_ADMIN|ROLE_USER_COMMERCIAL']], function () {
});