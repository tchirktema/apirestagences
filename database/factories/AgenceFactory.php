<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Agence;
use App\Models\Category;
use Faker\Generator as Faker;

$factory->define(Agence::class, function (Faker $faker) {
    $categoryCount = Category::all()->count();
    return  [
        'nom'=>$faker->sentence,
        'situation_geographique'=>$faker->address,
        'ville'=>$faker->city,
        'telephone'=> $faker->e164PhoneNumber,
        'longitude'=>$faker->longitude,
        'latitude'=>$faker->latitude,
        'category_id'=>random_int(1,$categoryCount),
        'image'=>$faker->sentence
    ];
});
