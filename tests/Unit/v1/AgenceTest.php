<?php

namespace Tests\Unit\v1;

use App\Models\Category;
use App\Models\Agence;
use Tests\TestCase;

class AgenceTest extends TestCase
{
    public function test_can_create_agence()
    {
        $categoryCount = Category::all()->count();
        $data = [
            'nom'=>$this->faker->sentence,
            'situation_geographique'=>$this->faker->address,
            'ville'=>$this->faker->city,
            'telephone'=> $this->faker->e164PhoneNumber,
            'longitude'=>$this->faker->longitude,
            'latitude'=>$this->faker->latitude,
            'category_id'=>random_int(1,$categoryCount),
            'image'=>$this->faker->sentence
        ];

        $this->post(route('agences.store'), $data)
            ->assertStatus(200);
    }

    public function test_can_update_category() {
        $categoryCount = Category::all()->count();
        $agence = factory(Agence::class)->create();

        $data = [
            'nom'=>$this->faker->sentence,
            'situation_geographique'=>$this->faker->address,
            'ville'=>$this->faker->city,
            'telephone'=> $this->faker->e164PhoneNumber,
            'longitude'=>$this->faker->longitude,
            'latitude'=>$this->faker->latitude,
            'category_id'=>random_int(1,$categoryCount),
            'image'=>$this->faker->sentence
        ];
        $this->put(route('agences.update', $agence->id), $data)
            ->assertStatus(200);
    }

    public function test_can_show_agence(){
        $agence = factory(Agence::class)->create();
        $this->get(route('agences.show', $agence->id))
            ->assertStatus(200);
    }

    public function test_can_delete_agence() {
        $agence = factory(Agence::class)->create();
        $this->delete(route('agences.destroy', $agence->id))
            ->assertStatus(200);
    }
}
