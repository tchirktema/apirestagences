<?php

namespace Tests\Unit\v1;

use App\Models\Category;
use Tests\TestCase;

class CategoryTest extends TestCase
{

    public function test_can_create_category() {
        $data = [
            'name' => 'Agence qwerty',
            'marker' => 'mmkfmdfmdfdmfdmfdkfdmfkm',
        ];
        $this->post(route('categories.store'), $data)
            ->assertStatus(200);
    }

    public function test_can_update_category() {
        $category = factory(Category::class)->create();
        $data = [
            'name' => $this->faker->sentence,
            'marker' => $this->faker->paragraph,
        ];
        $this->put(route('categories.update', $category->id), $data)
            ->assertStatus(200)
            ->assertJson($data);
    }

    public function test_can_show_category(){
        $category = factory(Category::class)->create();
        $this->get(route('categories.show', $category->id))
            ->assertStatus(200);
    }

    public function test_can_delete_category() {
        $category = factory(Category::class)->create();
        $this->delete(route('categories.destroy', $category->id))
            ->assertStatus(200);
    }
}
